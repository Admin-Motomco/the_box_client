import styled from 'styled-components';

export const Container = styled.div`
  padding: 15px;
  display: flex;
  width: 100%;
  justify-content: center;

  button {
    border-radius: 4px;
    border: none;
    padding: 5px;
    background: transparent;
    font-weight: bold;
    margin: 0 2px;
  }
`;
