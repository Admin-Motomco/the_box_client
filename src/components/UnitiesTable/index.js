import React, { useState } from 'react';
import ReactTable from 'react-table';
import { FaRegTrashAlt, FaRegEdit } from 'react-icons/fa';
import { Container } from './styles';
import '../react-table.css';

import api from '../../services/api';

export default function UnitiesTable({ company }) {
  const [unities, setUnities] = useState([]);

  const columns = [
    {
      Header: 'ID',
      accessor: 'id', // String-based value accessors!
    },
    {
      Header: 'Nome',
      accessor: 'name',
    },
    {
      Header: 'Telefone',
      accessor: 'phone',
    },
    {
      Header: 'Empresa',
      accessor: 'company.name',
    },
    {
      Header: 'Ações',
      Cell: row => (
        <div
          style={{
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'center',
          }}
        >
          <button type="button">
            <FaRegEdit size={20} />
          </button>
          <button type="button">
            <FaRegTrashAlt size={20} />
          </button>
        </div>
      ),
    },
  ];

  function getUnities() {
    api
      .get(`${process.env.REACT_APP_DEV_API_URL}/unities/companies/${company}`)
      .then(res => {
        setUnities(res.data);
      })
      .catch(err => {
        console.tron.log(err);
      });
  }

  return (
    <Container>
      <ReactTable
        data={unities}
        columns={columns}
        defaultPageSize={10}
        nextText="Próxima"
        previousText="Anterior"
        className="-striped -highlight"
        manual
        onFetchData={(state, instance) => {
          getUnities();
        }}
      />
    </Container>
  );
}
