import React from 'react';

import { Container, OptionButton } from './styles';

export default function UserOptions() {
  return (
    <Container>
      <OptionButton to="unities">Unidades</OptionButton>
      <OptionButton>Usuários</OptionButton>
      <OptionButton>Equipamentos</OptionButton>
      <OptionButton>Dashboard</OptionButton>
      <OptionButton>Relatórios</OptionButton>
      <OptionButton>Configurações</OptionButton>
    </Container>
  );
}
