import styled from 'styled-components';
import { Link } from 'react-router-dom';
import { darken } from 'polished';

export const Container = styled.div`
  padding: 30px;
  z-index: 10;
  display: grid;
  width: 100%;
  grid-gap: 20px;
  column-count: 2;
  grid-template-columns: repeat(3, 1fr);
`;

export const OptionButton = styled(Link)`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 15vh;
  border: solid #333;
  background: #fff;
  color: #666;
  font-weight: bold;
  font-size: 24px;
  border-radius: 6px;
  transition: all 0.4s;

  &:hover {
    color: #fff;
    background: #faa200;
    border: 0;
    border-radius: 50px;
    box-shadow: 6px 6px 6px #666;
  }
`;
