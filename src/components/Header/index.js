import React from 'react';
import { Link } from 'react-router-dom';

import { useDispatch } from 'react-redux';

import logo from '../../assets/images/header_logo.svg';
import logout from '../../assets/images/logout.svg';

import { signOut } from '../../store/modules/auth/actions';

import { Container, Content } from './styles';

export default function Header() {
  const dispatch = useDispatch();

  function handleExit() {
    dispatch(signOut());
  }

  return (
    <Container>
      <Content>
        <nav>
          <Link to="/home">
            <img src={logo} alt="thebox-management" />
          </Link>
        </nav>

        <button type="button" onClick={handleExit}>
          <aside>
            <img src={logout} alt="logout" />
            <strong>Sair</strong>
          </aside>
        </button>
      </Content>
    </Container>
  );
}
