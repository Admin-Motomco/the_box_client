import styled from 'styled-components';

export const Container = styled.div`
  background: #39393b;
  padding: 0 115px;
`;

export const Content = styled.div`
  height: 64px;
  display: flex;
  margin: 0 auto;
  justify-content: space-between;
  align-items: center;

  button {
    margin-left: 10px;
    background: transparent;
    color: #fff;
    font-size: 20px;
    border: none;
    font-weight: bold;
  }

  aside {
    display: flex;
    color: #fff;
    justify-content: center;
    align-items: center;
    font-size: 24px;

    strong {
      margin-left: 5px;
    }
  }
`;
