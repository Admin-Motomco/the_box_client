import styled, { keyframes } from 'styled-components';
import { darken } from 'polished';
import { fadeIn } from 'react-animations';

export const Bounce = styled.div`
  animation: 2s ${keyframes`${fadeIn}`};
`;

export const Container = styled.div`
  height: 100%;
  color: #808080;
  display: flex;
  flex-direction: column;

  width: 100%;
  max-width: 300px;
  min-height: 100%;
  padding-right: 20px;
  margin: 0 0 0 20px;
  border-right: 1px solid rgba(0, 0, 0, 0.1);
`;

export const Profile = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 100%;
  border-bottom: 2px solid rgba(0, 0, 0, 0.1);

  strong {
    font-size: 22px;
  }

  span {
    text-align: center;
    width: 100%;
    font-size: 18px;
    opacity: 0.5;
    margin: 10px 0;
  }

  img {
    border-radius: 50%;
    margin: 20px 0;
  }
`;

export const Actions = styled.div`
  margin: 30px 0;
`;

export const MenuTitle = styled.strong`
  font-size: 20px;
  opacity: 0.5;
`;
export const MenuItems = styled.ul`
  font-size: 18px;
  font-weight: bold;
  padding: 0 5px;

  ul {
    margin-left: 40px;
    margin-top: -20px;
  }

  a {
    text-decoration: none;
    color: #808080;
    display: flex;
    align-items: center;
    transition: background 0.4s;

    &:hover {
      background: ${darken(0.07, '#FFF')};
    }
  }

  > li {
    margin: 35px 10px;

    img {
      margin: 10px;
    }
  }
`;
export const Version = styled.div`
  margin-top: 15px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  flex-direction: row;

  span {
    display: flex;
    align-items: center;
    margin: 0px 10px;
    font-size: 18px;
    font-weight: bold;
    padding: 0 5px;

    img {
      margin: 10px;
    }
  }

  div {
    padding: 5px;
    background: #65b2e2;
    color: #fff;
    border-radius: 4px;
    margin: 15px 10px;
    font-size: 18px;
    font-weight: bold;
  }
`;

export const Button = styled.button`
  text-decoration: none;
  border: none;
  width: 100%;
  outline: none;
  color: #808080;
  display: flex;
  align-items: center;
  transition: background 0.4s;
  font-size: 18px;
  font-weight: bold;

  &:hover {
    background: ${darken(0.07, '#FFF')};
  }
`;

export const InsideItems = styled.li`
  margin-left: 15px;
`;
