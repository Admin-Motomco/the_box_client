import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import { useSelector } from 'react-redux';

import {
  Container,
  Profile,
  MenuTitle,
  MenuItems,
  Version,
  Actions,
} from './styles';

import history from '../../services/history';

import info from '../../assets/images/info.svg';

export default function Sidenav() {
  const [showMenu, setShowMenu] = useState(false);
  const { user } = useSelector(state => state.auth);

  return (
    <Container>
      <Profile>
        <img
          src="https://api.adorable.io/avatars/64/abott@adorable.png"
          alt="avatar"
        />
        {console.tron.log(user[0].company)}
        <strong>{user[0].name}</strong>
        <span>{user[0].company.name}</span>
      </Profile>
      <Actions>
        <MenuTitle>Páginas</MenuTitle>
        <MenuItems>
          <li>
            <Link to="profile">Perfil</Link>
          </li>
        </MenuItems>
        <MenuTitle>Suporte</MenuTitle>
        <Version>
          <span>
            <img src={info} alt="Versão" /> Versão
          </span>
          <div>
            <strong>1.0.0</strong>
          </div>
        </Version>
      </Actions>
    </Container>
  );
}
