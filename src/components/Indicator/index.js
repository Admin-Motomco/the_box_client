import React from 'react';
import PropTypes from 'prop-types';

import { Container, Bounce } from './styles';

export default function Indicator({ name }) {
  return (
    <Container>
      <Bounce>
        <h1>{name}</h1>
      </Bounce>
    </Container>
  );
}

Indicator.propTypes = {
  name: PropTypes.string.isRequired,
};
