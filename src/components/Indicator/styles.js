import styled, { keyframes } from 'styled-components';
import { bounceInLeft } from 'react-animations';

export const Container = styled.div`
  display: flex;
  margin-top: 30px;
  justify-content: center;
  padding-bottom: 15px;
  margin: 30px 15px;
  color: #808080;
  border-bottom: 1px solid rgba(0, 0, 0, 0.1);
`;

export const Bounce = styled.div`
  animation: 2s ${keyframes`${bounceInLeft}`};
`;
