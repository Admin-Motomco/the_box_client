import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
  width: 100%;
`;

export const AnimationContainer = styled.div`
  opacity: 65%;
  display: flex;
  justify-content: center;
`;
