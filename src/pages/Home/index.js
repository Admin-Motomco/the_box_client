import React from 'react';

import Lottie from 'react-lottie';
import { Container, AnimationContainer } from './styles';

import * as dashboard from '../../assets/lotties/dashboard.json';

import UserOptions from '../../components/UserOptions';

export default function Home() {
  const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: dashboard.default,
    rendererSettings: {
      preserveAspectRatio: 'xMidYMid slice',
    },
  };

  return (
    <Container>
      <UserOptions />
      <AnimationContainer>
        <Lottie options={defaultOptions} height="80%" width="80%" />
      </AnimationContainer>
    </Container>
  );
}
