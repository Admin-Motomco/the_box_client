import React from 'react';
import PropTypes from 'prop-types';

import Header from '../../../components/Header';
import Sidenav from '../../../components/Sidenav';

import { Wrapper, Content } from './styles';

export default function DefaultLayout({ children }) {
  return (
    <Wrapper>
      <Header />
      <Content>
        <Sidenav />
        {children}
      </Content>
    </Wrapper>
  );
}

DefaultLayout.propTypes = {
  children: PropTypes.element.isRequired,
};
