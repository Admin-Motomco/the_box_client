import React from 'react';

import { Container } from './styles';
import Indicator from '../../components/Indicator';
import UnitiesTable from '../../components/UnitiesTable';

export default function Unities() {
  return (
    <Container>
      <Indicator name="Unidades" />
      <UnitiesTable company="76" />
    </Container>
  );
}
